<?php

namespace Fibonacci;

class Cmd {
  public static function getFib() {
    $res = NULL;

    $fp = fopen("/tmp/fibonacci.txt", "r");
    if (!$fp) {
      $res = "no data";
    } else {
      if (flock($fp, LOCK_SH)) {
        $res = file("/tmp/fibonacci.txt", FILE_IGNORE_NEW_LINES);
        flock($fp, LOCK_UN);
      } else {
        $res = "no lock";
      }
      fclose($fp);
    }

    return $res;
  }
}
