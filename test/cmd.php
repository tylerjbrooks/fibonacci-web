<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Fibonacci\Cmd;

header("Content-Type:application/json");

if (!empty($_GET['cmd'])) {
  $cmd = $_GET['cmd'];
  if ($cmd == 'get') {
    response(200, "Fibonacci: ", Cmd::getFib());
  } else {
    response(200, "Command Not Recognized", NULL);
  }
} else {
  response(400, "Invalid Request", NULL);
}

function response($status, $status_message, $data) {
  header("HTTP/1.1 " . $status);
  $response['data'] = $data;
  $json_response = json_encode($response);
  echo $json_response;
}

